#!/usr/bin/env python3
import sys
import random


def prompt_players():
    """Find out how many players will be playing the game.   Keeps looping
    until the number of players is answered.  A valid answer is between 3 and 10 inclusive.

    Returns the number of players
    """
    while True:
        print("How many players are there (at least 3)?")
        # Grab the input and convert the string to an integer
        # (this will error if a non-integer is typed)
        answer = int(input('> '))
        # Make sure the answer is between 3 and 10 since we don't need 100 people playing
        if answer < 3 or answer > 10:
            print(
                "The number of players is outside of the acceptable range.  Please try again.")
        else:
            # Return the number of players
            return answer


def init_chips():
    """Loops through each player and gives them each 3 chips"""
    # We need to reference the global variable "chips" otherwise this will
    # create a new variable called chips inside this function.
    global chips
    # Loop through each player starting at 1 and ending at the max players + 1.
    # The reason we're going to max players + 1 is because the "chips" variable starts
    # at 1 instead of 0 and range ends at the number specified.
    for i in range(1, num_players + 1):
        chips[i] = 3


def roll_dice(player):
    """Rolls the dice for a player number"""
    # Figure out how many times the player needs to roll the dice.  The player will
    # roll the dice for each chip they have with a max roll of 3.
    roll_times = chips[player]
    if chips[player] >= 3:
        roll_times = 3

    # Roll the dice "roll_times" times
    for i in range(0, roll_times):
        # If the player doesn't have any more chips, return
        if chips[player] == 0:
            print("Player {player} has no more chips")
            return

        # Pick a random entry from the dice list
        roll = random.choice(dice)
        print("Player %s rolled '%s'" % (player, roll))
        # Call the "move_chips" function to handle moving a chip from the player according
        # to what they rolled
        move_chips(player, roll)


def move_chips(player, roll):
    """Given a player number and a roll value, move a chip from their pile
    if necessary."""
    # Again we need to reference the global "chips" variable since we will be making
    # modifications to it
    global chips

    # Go through each roll possibility and handle the chip move accordingly.
    # If the roll was L
    if roll == "L":
        print("  1 chip goes to the player on the left")
        # Remove a chip from the current player
        chips[player] -= 1
        # Assuming the player to the left of Player 1 is the highest player number.
        # For example, if the current player is Player 1 and there are 3 people playing, the
        # chip needs to go to Player 3.
        if player == 1:
            # Add a chip to the highest player number
            chips[num_players] += 1
        else:
            # Give a chip to the player directly below Player 1.
            # For example, if the current player is Player 3, give a chip to Player 2.
            chips[player - 1] += 1

    # If the roll was C
    elif roll == "C":
        print("  1 chip goes to the center pot")
        # Deduct a chip from the current player.  Since we're not keeping track of how many chips are
        # in the center pot, this is all we need to do.
        chips[player] -= 1

    # If the roll was R
    elif roll == "R":
        print("  1 chip goes to the player on the right")
        # Remove a chip from the current player
        chips[player] -= 1
        # Assuming the player to the right of the highest player number is Player 1 (rollover).
        # For example, if the current player is the last player, the player
        # to the right would be Player 1.
        if player == num_players:
            chips[1] += 1
        else:
            # Give a chip to the player above the current player.
            # For example, if the current player is Player 2, give a chip to Player 3.
            chips[player + 1] += 1


def print_chips():
    """Helper function to print out each player and the number of chips
    they have"""
    print("\n" + "-" * 18)
    print("Player    Chips")
    print("-" * 18)
    # Loop through each player and print their current chips
    for i in range(1, num_players + 1):
        # Use f strings to print out the values using specific field lengths
        # to mimic a table
        print(f"{i:<8} {chips[i]:2}")
    # Print a blank line
    print()


def winning_condition():
    """Check to see if there is a winner and exit if so"""
    # Reference the global variable turns
    global turns
    # Keep track of how many players have no chips
    players_out = 0
    # A variable to store the winning player number
    winning_player = 0
    # Loop through each player and count how many have 0 chips
    for i in range(1, num_players + 1):
        # If the current player has no chips
        if chips[i] == 0:
            # Increment "players_out" by 1
            players_out += 1
        # The current player has chips
        else:
            # While we're looping, put the player number with non-zero chips in this variable
            winning_player = i

    # If all players have 0 chips except for 1, print the winning player number
    if players_out == num_players - 1:
        print(f"Player {winning_player} wins after {turns} turns!!")
        print_chips()
        sys.exit(0)


def next_player(turn):
    """Figure out which player's turn it is.

    Returns the player whose turn it is.
    """
    turn += 1
    # If the turn exceeds the number of players, start back at 1
    if turn > num_players:
        turn = 1
    return turn


#
# MAIN
#

# Global variable to store the number of players
num_players = 0
# Global dictionary to store each player and the number of chips they have
# in the form of chips[<player number>] = <number of chips>
chips = {}
# Global list that stores each side of the dice
dice = ["L", "C", "R", "dot", "dot", "dot"]
# Extra variable to store how many total turns it took to win the game
turns = 0

# Counter to track which player's turn it is
player_turn = 1
# Get the number of players playing
num_players = prompt_players()

# Give each player their starting chips
init_chips()

# Game loop
while True:
    # Check if the current player has enough chips to roll the dice
    if chips[player_turn] == 0:
        print("Player %s has no chips.  Moving onto the next player" % player_turn)
        # Get the number of the next player
        player_turn = next_player(player_turn)
        # Move to the next interation of the loop
        continue

    # Keep track of the number of turns played
    turns += 1

    # Prompt loop to either show the current score or to roll the dice.
    # This is so the game doesn't move so fast you can't read anything.
    while True:
        print("Press any key to roll the dice or 'p' to print the score")
        # Get the answer to the prompt question
        answer = str(input('> '))
        # Print the current score when answering 'p'
        # Do case insensitive matching so we don't need to test for 'P' and 'p'.
        if answer.lower() == 'p':
            # Print the score
            print_chips()
        # If any other key is pressed besides 'p', break out of the loop and roll the dice.
        else:
            break

    # Roll the dice
    roll_dice(player_turn)

    # Check for winning condition
    winning_condition()

    # Get the number of the next player
    player_turn = next_player(player_turn)

    print("Next turn\n")
